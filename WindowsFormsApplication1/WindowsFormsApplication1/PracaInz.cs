﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PracaInz
{
    public partial class RozpoznawaniePisma : Form
    {
        
        Graphics grafika_panel,grafika_panel2, grafika_bitmapa;
        Bitmap b1, b2;
        Pen p1, p2;
        Pen pioro;
        List<int> lista_wzorcow;
        List<string> lista;
        Macierz macierz,macierz_wejscia,macierz_wyjscia;
        Plik plik;
        SSN siec;
        Brush b;
        string tmp;
        int m, n;
        bool painting;
        int h, w;
        int x, y;
        int lewy, prawy, gora, dol;
        bool rysowanie;
        bool zaznaczone;

        
        public RozpoznawaniePisma()
        {
            InitializeComponent();
            
            //Grafika w Panelach
            b1 = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            grafika_panel = Panel1.CreateGraphics();
            grafika_bitmapa = Graphics.FromImage(b1);
            rysowanie = false;


            pioro = new Pen(Color.Red);
            zaznaczone = false;

            

            grafika_panel2 = Panel2.CreateGraphics();
            


            
            //Piora do Paneli
            p1 = new Pen(Color.Black, 1);
            p2 = new Pen(Color.Blue, 1);
            b = new SolidBrush(Color.Aqua);
            
            //Tworzenie punktu
            Wspolrzedne pkt = new Wspolrzedne(0, 0);


            //Tworzenie listy wzorcow

            lista_wzorcow = new List<int>();
            Lista_wzorców();
            
           
            //macierz wejscia
            m = 10;
            n = 10;          
            macierz = new Macierz(m, n);
            
            painting = false;

            //Tworzenie sieci neuronowej

             macierz_wejscia = new Macierz(4, 9);                                                         //Macierz wejsciowa
             macierz_wyjscia = new Macierz(4, 4);

             for (int i = 0; i < 4; i++)
                 macierz_wyjscia.Wprowadz(i, i, 1);

            

           //Tworzenie wykresu
            Wykres.Series.Add("Blad sieci");


            //Tworzenie instancji plik
            plik = new Plik("plik.txt");
            plik.Czytaj();

        }






        public void RysujBitmape(Bitmap bmp,PictureBox pic)
        {
            b2 = SkalowanieBitmapy(b1);
            pic.Image = b2;
            b2.Save("mapa.bmp");
        }

        protected Bitmap SkalowanieBitmapy(Bitmap zrodlo)
        {
            Rectangle wycinanie = new Rectangle(lewy, gora, prawy - lewy + 4, dol - gora + 4);
            Rectangle rozmiar = new Rectangle(0, 0, 140, 140);
            Bitmap bmp = new Bitmap(pictureBox1.Width, pictureBox1.Height);
            Graphics g = Graphics.FromImage(bmp);
            g.DrawImage(zrodlo, rozmiar, wycinanie, GraphicsUnit.Pixel);
            g.Dispose();
            return bmp;
        }


        private void SprawdzPiksele(PictureBox panel, Bitmap bitmap, int m, int n)
        {
            int dlugosc = panel.Height / m;
            int szerokosc = panel.Width / n;

            for (int i = 0; i < panel.Width; i++)
                for (int j = 0; j < panel.Height; j++)
                    if (bitmap.GetPixel(i, j).A > 200)
                        macierz.Wprowadz((j / dlugosc), (i / szerokosc), 1);


        }





        private void Ustawpunkty(int x, int y)
        {
            if (!zaznaczone)
            {
                lewy = x;
                prawy = x;
                gora = y;
                dol = y;
                zaznaczone = true;
            }


            if (x < lewy)
                lewy = x;
            if (x > prawy)
                prawy = x;
            if (y > dol)
                dol = y;
            if (y < gora)
                gora = y;


        }

       


        private void RysujRamke(Graphics g, int lewo, int prawo, int gora, int dol, int dlugosc)
        {
            g.DrawRectangle(pioro, lewo, gora, prawo - lewo + dlugosc, dol - gora + dlugosc);

        }

        private void Panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (rysowanie)
            {
                Brush b = new SolidBrush(Color.Black);
                grafika_panel.FillEllipse(b, e.X, e.Y, 5, 5);
                grafika_bitmapa.FillEllipse(b, e.X, e.Y, 5, 5);

                Ustawpunkty(e.X, e.Y);


            }
        }


        

        private void Panel1_MouseUp(object sender, MouseEventArgs e)
        {
            rysowanie = false;
            RysujRamke(grafika_panel, lewy, prawy, gora, dol, 6);
            RysujBitmape(b1,pictureBox1);
            SprawdzPiksele(pictureBox1, b2, m, n);
            macierz_wys.Text = macierz.Wyswietl_macierz();

        }

        private void Panel1_MouseDown(object sender, MouseEventArgs e)
        {
            rysowanie = true;
            macierz.Zeruj();
        }

        private void B_rozpoznaj_Click(object sender, EventArgs e)
        {
            string odpowiedzi="";

            Panel1.Refresh();
            grafika_bitmapa.Clear(Color.Transparent);
            pictureBox1.Refresh();
            zaznaczone = false;

            macierz.Zamien_na_wektor();
            siec.Oblicz_wyjscia(macierz.wektor);

            foreach (var v in siec.Y2)
                odpowiedzi += v.ToString()+Environment.NewLine;

            L_wyjscia.Text = odpowiedzi;

            macierz.Zeruj();
        }


        



        private void Panel2_MouseMove(object sender, MouseEventArgs e)
        {
            if (rysowanie)
            {
                Brush b = new SolidBrush(Color.Black);
                grafika_panel2.FillEllipse(b, e.X, e.Y, 5, 5);
                grafika_bitmapa.FillEllipse(b, e.X, e.Y, 5, 5);

                Ustawpunkty(e.X, e.Y);


            }
        }


        private void Panel2_MouseUp(object sender, MouseEventArgs e)
        {
            rysowanie = false;
            RysujRamke(grafika_panel2, lewy, prawy, gora, dol, 1);
            RysujBitmape(b1,pictureBox2);
            SprawdzPiksele(pictureBox2, b2, m, n);
            macierz_wys.Text = macierz.Wyswietl_macierz();

        }

        private void Panel2_MouseDown(object sender, MouseEventArgs e)
        {
            rysowanie = true;
        }

       
       


        private void Rysuj_wykres(Color kolor)
        {
            Wykres.Series["Blad sieci"].Points.Clear();
            Wykres.Series["Blad sieci"].Color = kolor;
            Wykres.Series["Blad sieci"].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;


            foreach (var w in siec.przebieg_uczenia)
                Wykres.Series["Blad sieci"].Points.AddXY(w.x, w.y1);



        }




        private void B_Uczenie_Click(object sender, EventArgs e)
        {
            plik.Czytaj();
            siec = new SSN(m*n, 10, 10);
            siec.Wsteczna_propagacja(plik.wejscia, plik.wyjscia, Convert.ToDouble(Tb_blad.Text),Convert.ToDouble(Tb_wsp.Text),0.1);
            //siec.Wsteczna_propagacja(macierz_wejscia, macierz_wyjscia, Convert.ToDouble(Tb_blad.Text), Convert.ToDouble(Tb_wsp.Text));


            Dodaj_do_listy();
            Rysuj_wykres(Color.Green);
        }


        private void Lista_wzorców()
        {
            for (int i = 0; i < 10; i++)
                lista_wzorcow.Add(i);

            
            L_wzorcow.DataSource = lista_wzorcow;
        }


        private void Macierz_wyjsciowa()
        {
            for (int i = 0; i < 10;i++)
                macierz_wyjscia.Wprowadz(i, i, 1);

        }

        private void Tb_blad_KeyPress(object sender, KeyPressEventArgs e)
        {
             if(!Char.IsNumber(e.KeyChar) && e.KeyChar!=',' && e.KeyChar!='\b')
                e.Handled = true;

             
             if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
             {
                 e.Handled = true;
             }
        }

        private void Tb_wsp_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsNumber(e.KeyChar) && e.KeyChar != ',' && e.KeyChar != '\b')
                e.Handled = true;


            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }
        }

        private void L_wzorcow_SelectedIndexChanged(object sender, EventArgs e)
        {
            tmp="";

            for (int i = 0; i < 10; i++)
                if (i.Equals(L_wzorcow.SelectedIndex))
                    tmp += "1";
                else
                    tmp += "0";

            L_wektor_wyjsciowy.Text = "Wektor wyjściowy: " + tmp;


        }

        private void B_wprowadz_Click(object sender, EventArgs e)
        {
            Panel2.Refresh();

            grafika_bitmapa.Clear(Color.Transparent);
            pictureBox2.Refresh();
            zaznaczone = false;
            
            L_wektor_wejsciowy.Text = "Wektor wejściowy: " + macierz.Wyswietl_macierz();

            plik.Czytaj();
            plik.Zapisz_do_pliku(macierz.Wyswietl_wektor(),tmp);

            macierz.Zeruj();
        }




        private void Dodaj_do_listy()
        {
            lista = new List<string>();

            foreach (var l in siec.przebieg_uczenia)
                lista.Add(l.Wsp_graf());

            Przebieg_uczenia.DataSource = lista;
        }

        /*private void Rysuj_siatke(Panel panel, Graphics g, Pen p)
        {

            h = panel.Height / m;
            w = panel.Width / n;

            for (int i = 1; i < m; i++)
            {
                g.DrawLine(p, i * w, 0, i * w, panel.Height);
                g.DrawLine(p, 0, i * h, panel.Width, i * h);
            }


        }*/


        private void Ustaw_wsp(int x, int y, Panel panel)
        {
            if (x >= panel.Width - n)
                this.x = panel.Width - n;
            else if (x < 0)
                this.x = 0;
            else if (y >= panel.Height - m)
                this.y = panel.Height - m;
            else if (y < 0)
                this.y = 0;
            else
            {
                this.x = x;
                this.y = y;
            }



        }

        private void Strona1_Click(object sender, EventArgs e)
        {

        }

       

       

        

       
        
    }
}