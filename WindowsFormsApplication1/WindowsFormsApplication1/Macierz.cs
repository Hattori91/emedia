﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PracaInz
{

    class Macierz
    {

        private int[,] macierz;
        public double[] wektor;
        public int m, n;
        private string s;


        public Macierz(int m, int n)
        {
            this.m = m;
            this.n = n;
            macierz = new int[m, n];
            wektor = new double[m * n];

        }


        public double[] Wiersz(int numer)
        {
            double[] wiersz = new double[n];

            for (int j = 0; j < n; j++)
                wiersz[j] = (double)macierz[numer, j];

            return wiersz;
        }



        public void Wprowadz(int i, int j, int wartosc)
        {
            macierz[i, j] = wartosc;
        }


        public void Zamien_na_wektor()
        {
            int k = 0;

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                {
                    wektor[k] = (double)macierz[i, j];
                    k++;
                }

        }


        public void Zeruj()
        {
            Array.Clear(macierz, 0, macierz.Length);
        }


        public  string Wyswietl_macierz()
        {
            s = "";

            for (int i = 0; i < m; i++)
            {
                s += "\n";

                for (int j = 0; j < n; j++)
                {
                    s += macierz[i, j].ToString();
                    s += " ";
                }
            }

            return s;
        }

        public string Wyswietl_wektor()
        {
            s = "";

            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    s += macierz[i, j].ToString();
                    s += "";
                }
            }

            return s;
        }






    }
}
