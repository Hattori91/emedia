﻿namespace PracaInz
{
    partial class RozpoznawaniePisma
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.Strona1 = new System.Windows.Forms.TabPage();
            this.B_rozpoznaj = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.macierz_wys = new System.Windows.Forms.Label();
            this.L_wyjscia = new System.Windows.Forms.Label();
            this.Pozycja = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Strona2 = new System.Windows.Forms.TabPage();
            this.L_wsp = new System.Windows.Forms.Label();
            this.L_blad = new System.Windows.Forms.Label();
            this.Tb_wsp = new System.Windows.Forms.TextBox();
            this.Tb_blad = new System.Windows.Forms.TextBox();
            this.Wykres = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.B_Uczenie = new System.Windows.Forms.Button();
            this.Przebieg_uczenia = new System.Windows.Forms.ListBox();
            this.Strona3 = new System.Windows.Forms.TabPage();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.Panel2 = new System.Windows.Forms.Panel();
            this.L_wektor_wejsciowy = new System.Windows.Forms.Label();
            this.L_wektor_wyjsciowy = new System.Windows.Forms.Label();
            this.L_wzorcow = new System.Windows.Forms.ListBox();
            this.B_wprowadz = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.TabControl.SuspendLayout();
            this.Strona1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.Strona2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Wykres)).BeginInit();
            this.Strona3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.Strona1);
            this.TabControl.Controls.Add(this.Strona2);
            this.TabControl.Controls.Add(this.Strona3);
            this.TabControl.Location = new System.Drawing.Point(12, 12);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(681, 384);
            this.TabControl.TabIndex = 0;
            // 
            // Strona1
            // 
            this.Strona1.Controls.Add(this.groupBox3);
            this.Strona1.Controls.Add(this.B_rozpoznaj);
            this.Strona1.Controls.Add(this.groupBox2);
            this.Strona1.Controls.Add(this.groupBox1);
            this.Strona1.Controls.Add(this.Pozycja);
            this.Strona1.Controls.Add(this.label2);
            this.Strona1.Location = new System.Drawing.Point(4, 22);
            this.Strona1.Name = "Strona1";
            this.Strona1.Padding = new System.Windows.Forms.Padding(3);
            this.Strona1.Size = new System.Drawing.Size(673, 358);
            this.Strona1.TabIndex = 0;
            this.Strona1.Text = "Rozpoznawanie";
            this.Strona1.UseVisualStyleBackColor = true;
            this.Strona1.Click += new System.EventHandler(this.Strona1_Click);
            // 
            // B_rozpoznaj
            // 
            this.B_rozpoznaj.Location = new System.Drawing.Point(209, 283);
            this.B_rozpoznaj.Name = "B_rozpoznaj";
            this.B_rozpoznaj.Size = new System.Drawing.Size(246, 45);
            this.B_rozpoznaj.TabIndex = 53;
            this.B_rozpoznaj.Text = "Rozpoznaj";
            this.B_rozpoznaj.UseVisualStyleBackColor = true;
            this.B_rozpoznaj.Click += new System.EventHandler(this.B_rozpoznaj_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureBox1);
            this.groupBox2.Controls.Add(this.Panel1);
            this.groupBox2.Location = new System.Drawing.Point(20, 25);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(345, 217);
            this.groupBox2.TabIndex = 40;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Rozpoznawanie Pisma";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Location = new System.Drawing.Point(174, 33);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(150, 150);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // Panel1
            // 
            this.Panel1.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel1.Location = new System.Drawing.Point(6, 33);
            this.Panel1.Name = "Panel1";
            this.Panel1.Size = new System.Drawing.Size(162, 150);
            this.Panel1.TabIndex = 53;
            this.Panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel1_MouseDown);
            this.Panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel1_MouseMove);
            this.Panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel1_MouseUp);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.macierz_wys);
            this.groupBox1.Location = new System.Drawing.Point(530, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(132, 217);
            this.groupBox1.TabIndex = 51;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Binearyzacja cyfry";
            // 
            // macierz_wys
            // 
            this.macierz_wys.AutoSize = true;
            this.macierz_wys.Location = new System.Drawing.Point(35, 29);
            this.macierz_wys.Name = "macierz_wys";
            this.macierz_wys.Size = new System.Drawing.Size(67, 13);
            this.macierz_wys.TabIndex = 39;
            this.macierz_wys.Text = "Binearyzacja";
            // 
            // L_wyjscia
            // 
            this.L_wyjscia.AutoSize = true;
            this.L_wyjscia.Location = new System.Drawing.Point(13, 29);
            this.L_wyjscia.Name = "L_wyjscia";
            this.L_wyjscia.Size = new System.Drawing.Size(44, 13);
            this.L_wyjscia.TabIndex = 52;
            this.L_wyjscia.Text = "Wyjścia";
            // 
            // Pozycja
            // 
            this.Pozycja.AutoSize = true;
            this.Pozycja.Location = new System.Drawing.Point(17, 299);
            this.Pozycja.Name = "Pozycja";
            this.Pozycja.Size = new System.Drawing.Size(66, 13);
            this.Pozycja.TabIndex = 38;
            this.Pozycja.Text = "Nie wybrano";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 259);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 35;
            this.label2.Text = "False";
            // 
            // Strona2
            // 
            this.Strona2.Controls.Add(this.L_wsp);
            this.Strona2.Controls.Add(this.L_blad);
            this.Strona2.Controls.Add(this.Tb_wsp);
            this.Strona2.Controls.Add(this.Tb_blad);
            this.Strona2.Controls.Add(this.Wykres);
            this.Strona2.Controls.Add(this.B_Uczenie);
            this.Strona2.Controls.Add(this.Przebieg_uczenia);
            this.Strona2.Location = new System.Drawing.Point(4, 22);
            this.Strona2.Name = "Strona2";
            this.Strona2.Padding = new System.Windows.Forms.Padding(3);
            this.Strona2.Size = new System.Drawing.Size(673, 358);
            this.Strona2.TabIndex = 1;
            this.Strona2.Text = "Uczenie";
            this.Strona2.UseVisualStyleBackColor = true;
            // 
            // L_wsp
            // 
            this.L_wsp.AutoSize = true;
            this.L_wsp.Location = new System.Drawing.Point(318, 20);
            this.L_wsp.Name = "L_wsp";
            this.L_wsp.Size = new System.Drawing.Size(78, 13);
            this.L_wsp.TabIndex = 6;
            this.L_wsp.Text = "Współczynnik:";
            // 
            // L_blad
            // 
            this.L_blad.AutoSize = true;
            this.L_blad.Location = new System.Drawing.Point(195, 20);
            this.L_blad.Name = "L_blad";
            this.L_blad.Size = new System.Drawing.Size(57, 13);
            this.L_blad.TabIndex = 5;
            this.L_blad.Text = "Błąd sieci:";
            // 
            // Tb_wsp
            // 
            this.Tb_wsp.Location = new System.Drawing.Point(402, 17);
            this.Tb_wsp.Name = "Tb_wsp";
            this.Tb_wsp.Size = new System.Drawing.Size(56, 20);
            this.Tb_wsp.TabIndex = 4;
            this.Tb_wsp.Text = "0,5";
            this.Tb_wsp.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tb_wsp_KeyPress);
            // 
            // Tb_blad
            // 
            this.Tb_blad.Location = new System.Drawing.Point(258, 17);
            this.Tb_blad.Name = "Tb_blad";
            this.Tb_blad.Size = new System.Drawing.Size(54, 20);
            this.Tb_blad.TabIndex = 3;
            this.Tb_blad.Text = "0,1";
            this.Tb_blad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Tb_blad_KeyPress);
            // 
            // Wykres
            // 
            chartArea1.Name = "ChartArea1";
            this.Wykres.ChartAreas.Add(chartArea1);
            this.Wykres.Location = new System.Drawing.Point(3, 60);
            this.Wykres.Name = "Wykres";
            this.Wykres.Size = new System.Drawing.Size(664, 191);
            this.Wykres.TabIndex = 2;
            this.Wykres.Text = "Wykres";
            // 
            // B_Uczenie
            // 
            this.B_Uczenie.Location = new System.Drawing.Point(25, 17);
            this.B_Uczenie.Name = "B_Uczenie";
            this.B_Uczenie.Size = new System.Drawing.Size(153, 32);
            this.B_Uczenie.TabIndex = 1;
            this.B_Uczenie.Text = "Uczenie sieci";
            this.B_Uczenie.UseVisualStyleBackColor = true;
            this.B_Uczenie.Click += new System.EventHandler(this.B_Uczenie_Click);
            // 
            // Przebieg_uczenia
            // 
            this.Przebieg_uczenia.FormattingEnabled = true;
            this.Przebieg_uczenia.Location = new System.Drawing.Point(6, 257);
            this.Przebieg_uczenia.Name = "Przebieg_uczenia";
            this.Przebieg_uczenia.Size = new System.Drawing.Size(664, 95);
            this.Przebieg_uczenia.TabIndex = 0;
            // 
            // Strona3
            // 
            this.Strona3.Controls.Add(this.pictureBox2);
            this.Strona3.Controls.Add(this.Panel2);
            this.Strona3.Controls.Add(this.L_wektor_wejsciowy);
            this.Strona3.Controls.Add(this.L_wektor_wyjsciowy);
            this.Strona3.Controls.Add(this.L_wzorcow);
            this.Strona3.Controls.Add(this.B_wprowadz);
            this.Strona3.Location = new System.Drawing.Point(4, 22);
            this.Strona3.Name = "Strona3";
            this.Strona3.Size = new System.Drawing.Size(673, 358);
            this.Strona3.TabIndex = 2;
            this.Strona3.Text = "Wprowadzanie wzorców";
            this.Strona3.UseVisualStyleBackColor = true;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox2.Location = new System.Drawing.Point(190, 17);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(150, 150);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // Panel2
            // 
            this.Panel2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Panel2.Location = new System.Drawing.Point(22, 17);
            this.Panel2.Name = "Panel2";
            this.Panel2.Size = new System.Drawing.Size(162, 150);
            this.Panel2.TabIndex = 55;
            this.Panel2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Panel2_MouseDown);
            this.Panel2.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel2_MouseMove);
            this.Panel2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Panel2_MouseUp);
            // 
            // L_wektor_wejsciowy
            // 
            this.L_wektor_wejsciowy.AutoSize = true;
            this.L_wektor_wejsciowy.Location = new System.Drawing.Point(19, 241);
            this.L_wektor_wejsciowy.Name = "L_wektor_wejsciowy";
            this.L_wektor_wejsciowy.Size = new System.Drawing.Size(96, 13);
            this.L_wektor_wejsciowy.TabIndex = 49;
            this.L_wektor_wejsciowy.Text = "Wektor wejściowy:";
            // 
            // L_wektor_wyjsciowy
            // 
            this.L_wektor_wyjsciowy.AutoSize = true;
            this.L_wektor_wyjsciowy.Location = new System.Drawing.Point(19, 195);
            this.L_wektor_wyjsciowy.Name = "L_wektor_wyjsciowy";
            this.L_wektor_wyjsciowy.Size = new System.Drawing.Size(95, 13);
            this.L_wektor_wyjsciowy.TabIndex = 48;
            this.L_wektor_wyjsciowy.Text = "Wektor wyjściowy:";
            // 
            // L_wzorcow
            // 
            this.L_wzorcow.FormattingEnabled = true;
            this.L_wzorcow.Location = new System.Drawing.Point(355, 17);
            this.L_wzorcow.Name = "L_wzorcow";
            this.L_wzorcow.Size = new System.Drawing.Size(147, 147);
            this.L_wzorcow.TabIndex = 47;
            this.L_wzorcow.SelectedIndexChanged += new System.EventHandler(this.L_wzorcow_SelectedIndexChanged);
            // 
            // B_wprowadz
            // 
            this.B_wprowadz.Location = new System.Drawing.Point(523, 314);
            this.B_wprowadz.Name = "B_wprowadz";
            this.B_wprowadz.Size = new System.Drawing.Size(147, 41);
            this.B_wprowadz.TabIndex = 46;
            this.B_wprowadz.Text = "Wprowadz wzorzec";
            this.B_wprowadz.UseVisualStyleBackColor = true;
            this.B_wprowadz.Click += new System.EventHandler(this.B_wprowadz_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.L_wyjscia);
            this.groupBox3.Location = new System.Drawing.Point(371, 25);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(153, 217);
            this.groupBox3.TabIndex = 54;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Wyjścia sieci";
            // 
            // RozpoznawaniePisma
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(698, 408);
            this.Controls.Add(this.TabControl);
            this.Name = "RozpoznawaniePisma";
            this.Text = "Rozpoznawanie_Pisma Projekt Inżynierski";
            this.TabControl.ResumeLayout(false);
            this.Strona1.ResumeLayout(false);
            this.Strona1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.Strona2.ResumeLayout(false);
            this.Strona2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Wykres)).EndInit();
            this.Strona3.ResumeLayout(false);
            this.Strona3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage Strona1;
        private System.Windows.Forms.TabPage Strona2;
        private System.Windows.Forms.Label macierz_wys;
        private System.Windows.Forms.Label Pozycja;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox Przebieg_uczenia;
        private System.Windows.Forms.Button B_Uczenie;
        private System.Windows.Forms.DataVisualization.Charting.Chart Wykres;
        private System.Windows.Forms.Label L_wsp;
        private System.Windows.Forms.Label L_blad;
        private System.Windows.Forms.TextBox Tb_wsp;
        private System.Windows.Forms.TextBox Tb_blad;
        private System.Windows.Forms.TabPage Strona3;
        private System.Windows.Forms.ListBox L_wzorcow;
        private System.Windows.Forms.Button B_wprowadz;
        private System.Windows.Forms.Label L_wektor_wyjsciowy;
        private System.Windows.Forms.Label L_wektor_wejsciowy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label L_wyjscia;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Panel Panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button B_rozpoznaj;
        private System.Windows.Forms.Panel Panel2;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox3;




    }
}

