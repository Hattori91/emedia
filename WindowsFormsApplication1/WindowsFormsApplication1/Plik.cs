﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;

namespace PracaInz
{ 
    class Plik
    {

           
            string tmp;
            string sciezka;
            string tekst;
            int liczbawejsc;
            int liczbawyjsc;
            public Macierz wejscia;
            public Macierz wyjscia;
            int ilosclini;
            
        

        public Plik(string adres)
        {
            sciezka = adres;
            tekst="";
            liczbawejsc = 100;
            liczbawyjsc = 10;

        }


        private int Ilosclini()
        {
            ilosclini = 0;

            using (StreamReader czytaj = new StreamReader(sciezka))
            {
                while ((tmp = czytaj.ReadLine()) != null)
                    ilosclini++;
            }

            return ilosclini;
        }
        


        public void Czytaj()
        {
           int licz=0;

           Ilosclini();

           if (ilosclini.Equals(0))
               return;

           wejscia = new Macierz(ilosclini, liczbawejsc);
           wyjscia = new Macierz(ilosclini, liczbawyjsc);

            using (StreamReader czytaj = new StreamReader(sciezka))
            {
                while ((tmp = czytaj.ReadLine()) != null)
                {
                    for (int i = 0; i < liczbawejsc; i++)
                        wejscia.Wprowadz(licz, i, Convert.ToInt32(tmp[i])-48);
                    for (int i = 0; i < liczbawyjsc;i++)
                        wyjscia.Wprowadz(licz, i, Convert.ToInt32(tmp[i+liczbawejsc])-48);

                        tekst += (tmp + Environment.NewLine);

                        licz++;
                    
                }
            }


        }


        public void Zapisz_do_pliku(string wejscie, string wyjscie)
        {
            tekst = "";

            Czytaj();

            using (StreamWriter zapisz = new StreamWriter(sciezka))
            {

                tekst += (wejscie + wyjscie);

                    zapisz.WriteLine(tekst);

            }
            

        }
            

 

    }
}
