﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ssn_test
{
    

    
    class SSN
    {
        public Neuron[] Warstwa_ukryta;
        public Neuron[] Warstwa_wyjsciowa;
        public List<string> przebieg_uczenia;
 
        public double[] Y1;
        public double[] Y2;
        public double[] D1;
        public double[] D2;
        public double[] E1;
        public double[] E2;

        
   

        public SSN(int wej, int ukr , int wyj)
        {
            Warstwa_ukryta = new Neuron[ukr];
            Warstwa_wyjsciowa = new Neuron[wyj];

            przebieg_uczenia = new List<string>();

            Buduj_siec(wej , ukr, wyj);

         
            Y1 = new double[ukr];
            Y2 = new double[wyj];
            D1 = new double[ukr];
            E1 = new double[ukr];
            E2 = new double[wyj];
            D2 = new double[wyj];



        }


        private void  Buduj_siec(int wejscie, int ukryta, int wyjscie)
        {

            for (int i = 0; i < ukryta; i++)
                Warstwa_ukryta[i] = new Neuron(wejscie,-1,5);

            for (int i = 0; i < wyjscie; i++)
                Warstwa_wyjsciowa[i] = new Neuron(ukryta,-1,5);
            
        }


        



        public void Uczenie(double[]X, double[]oczekiwane, double n)
        {

                Oblicz_wyjscia(X);
                Aktualizacja_wag(oczekiwane, X, n);

        }


        public void Wsteczna_propagacja(Macierz macierz_wejscia,Macierz macierz_wyjscia,double granica_bledu, double wsp)
        {
            

            double blad_sieci;

            int licz=0;

            int licz2=0;

            do{
            
                blad_sieci = 0;

                licz++;

                licz2++;

                /*if(licz2.Equals(licz2%1000))
                    wsp += 0.05;*/
                    
               

                for (int i = 0; i < macierz_wejscia.m; i++)
                    Uczenie(macierz_wejscia.Wiersz(i), macierz_wyjscia.Wiersz(i), wsp);

           
                for (int i = 0; i < macierz_wejscia.m; i++)
                {
                    Oblicz_wyjscia(macierz_wejscia.Wiersz(i));
                    blad_sieci+=Blad_wyjscia(macierz_wyjscia.Wiersz(i));
                }

           
               
                blad_sieci = blad_sieci / (Warstwa_wyjsciowa.Length*macierz_wejscia.m);

                blad_sieci = Math.Sqrt(blad_sieci);


                przebieg_uczenia.Add("Epoka: "+ licz.ToString()+" Blad sieci: " +  blad_sieci.ToString());

                Console.WriteLine("Epoka: {0}, Blad: {1}", licz, blad_sieci);
   
            }while(blad_sieci>granica_bledu && licz<100000);



        }

        public void Oblicz_wyjscia(double[]X)
        {

            for (int i = 0; i < Warstwa_ukryta.Length; i++)
                Y1[i] = Warstwa_ukryta[i].Wyjscie(X);

            for (int i = 0; i < Warstwa_wyjsciowa.Length; i++)
                Y2[i] = Warstwa_wyjsciowa[i].Wyjscie(Y1);
               
        }

        private void Aktualizacja_wag(double[]oczekiwane,double[]X, double n)
        {


            for (int i = 0; i < Warstwa_wyjsciowa.Length; i++)
            {
                D2[i] = oczekiwane[i] - Y2[i];
                E2[i] = Warstwa_wyjsciowa[0].beta * D2[i] * Y2[i] * (1 - Y2[i]);

            }

           double suma=0;

           for (int i = 0; i < Warstwa_ukryta.Length; i++)
           {
               for (int j = 0; j < Warstwa_wyjsciowa.Length; j++)
                   suma += E2[j] * Warstwa_wyjsciowa[j].W[i + 1];

               D1[i] = suma;
               E1[i] = Warstwa_wyjsciowa[0].beta * D1[i] * Y1[i] * (1 - Y1[i]);
               
               suma = 0;
           }



           for (int i = 0; i < Warstwa_ukryta.Length; i++)
           {
               for (int j = 1; j < Warstwa_ukryta[0].W.Length; j++)
                   Warstwa_ukryta[i].W[j] += n * E1[i] * X[j - 1];

               Warstwa_ukryta[i].W[0] += (n * E1[i] * Warstwa_ukryta[i].bias); 
           }



           for (int i = 0; i < Warstwa_wyjsciowa.Length; i++)
           {
               for (int j = 1; j < Warstwa_wyjsciowa[0].W.Length; j++)
                   Warstwa_wyjsciowa[i].W[j] += n * E2[i] * Y1[j - 1];

               Warstwa_wyjsciowa[i].W[0] += (n * E2[i] * Warstwa_wyjsciowa[i].bias); 

           }

             




        }




        private double Blad_wyjscia(double[] oczekiwane)
        {
            double suma = 0;

            for (int i = 0; i < Y2.Length; i++)
                suma += Math.Pow(oczekiwane[i] - Y2[i],2);

            return suma;
        }


        public double[] Wyjscia()
        {
            double[] wyjscia = new double[Y2.Length];

            for(int i=0;i<Y2.Length;i++)
                wyjscia[i] = Y2[i];

            return wyjscia;
        }

     
   
     


      
        

       
       


    }
}
