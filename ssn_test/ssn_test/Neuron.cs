﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ssn_test
{

    class Neuron
    {
        public double y;
        public double[]W;
        private int ilosc;
        public double bias;
        public double beta;
     


        public Neuron(int ile,double bias,double beta)
        {
            ilosc = ile;
            this.bias = bias;
            this.beta = beta;


            Wagi_Losowo();

                    
        }


        


        public double Wyjscie(double[]X)
        {
            
          y = Funkcja_Sigmoidalna(Suma(X));

            return y;

        }


        private double Funkcja_Sigmoidalna(double x) { return 1 / (1 + Math.Exp((-beta)*x)); }

        public double Pochodna_Funkcji_Sigm(double x) { return Funkcja_Sigmoidalna(x) * (1 - Funkcja_Sigmoidalna(x)); }


      

        private void Wagi_Losowo()
        {
            double z;

            W = new double[ilosc+1];

            var rand = new Random(Guid.NewGuid().GetHashCode());


            for (int i = 0; i < W.Length; i++)
            {
                z = rand.Next(-100,100) * 0.01 ;
                
                if (z != 0)
                    W[i] = z;
                else
                    W[i] = 0.015;
                
            }
            

        }

       
        public double Suma(double[]X)
        {
            double suma=0;


            for (int i = 1; i < W.Length; i++)
                suma += W[i] * X[i-1];

            suma += bias;
            
            return suma;
        }
     

      



    }
}
