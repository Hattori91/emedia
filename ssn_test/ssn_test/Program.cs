﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ssn_test
{

    class Program
    {
        static void Main(string[] args)
        {

             
            
           Macierz macierz_wejscia = new Macierz(4, 9);                                                         //Macierz wejsciowa
           Macierz macierz_wyjscia = new Macierz(4, 4);

           macierz_wejscia.Wprowadz(0,0,1);
           macierz_wejscia.Wprowadz(0, 2, 1);
           macierz_wejscia.Wprowadz(0, 4, 1);
           macierz_wejscia.Wprowadz(0, 6, 1);
           macierz_wejscia.Wprowadz(0, 8, 1);
           macierz_wejscia.Wprowadz(1, 0, 1);
           macierz_wejscia.Wprowadz(1, 1, 1);
           macierz_wejscia.Wprowadz(1, 2, 1);
           macierz_wejscia.Wprowadz(1, 3, 1);
           macierz_wejscia.Wprowadz(1, 5, 1);
           macierz_wejscia.Wprowadz(1, 6, 1);
           macierz_wejscia.Wprowadz(1, 7, 1);
           macierz_wejscia.Wprowadz(1, 8, 1);
           macierz_wejscia.Wprowadz(2, 1, 1);
           macierz_wejscia.Wprowadz(2, 3, 1);
           macierz_wejscia.Wprowadz(2, 4, 1);
           macierz_wejscia.Wprowadz(2, 5, 1);
           macierz_wejscia.Wprowadz(2, 7, 1);
           macierz_wejscia.Wprowadz(3, 3, 1);
           macierz_wejscia.Wprowadz(3, 4, 1);
           macierz_wejscia.Wprowadz(3, 5, 1);

           Macierz macierz_testowa = new Macierz(1, 9);

           macierz_testowa.Wprowadz(0, 1, 1);
           macierz_testowa.Wprowadz(0, 6, 1);
       
          



           for (int i = 0; i < 4; i++)
               macierz_wyjscia.Wprowadz(i, i, 1);

              
            Console.Write(macierz_wejscia.ToString());
            Console.Write(macierz_wyjscia.ToString());


            var siec = new SSN(9,5,4);                            //Siec neuronowa

            

            siec.Wsteczna_propagacja(macierz_wejscia,macierz_wyjscia,0.1,0.8);

            for (int i = 0; i < 4; i++)
            {
                siec.Oblicz_wyjscia(macierz_wejscia.Wiersz(i));
                
                foreach(var w in siec.Y2)
                    Console.WriteLine(w);
                
                Console.WriteLine("");
            }

            siec.Oblicz_wyjscia(macierz_testowa.Wiersz(0));
            foreach (var w in siec.Y2)
                Console.WriteLine(w);

            Console.WriteLine("");

            Console.ReadKey();
        }
    }
}
