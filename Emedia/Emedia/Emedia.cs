﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Emedia
{
    public partial class Emedia : Form
    {
        public Emedia()
        {
            InitializeComponent();
            this.MaximumSize = this.Size;
            this.MinimumSize = this.Size; 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog okienko = new OpenFileDialog();
            okienko.Filter = "Pliki bmp |*.bmp";
            if (okienko.ShowDialog() == DialogResult.OK)
            {

                try
                {

                    BMP objekt = new BMP();
                    objekt.CzytajPlik(okienko.FileName); //Odczytanie pliku i skopiowanie bajtow do tablicy buffer
                    objekt.Naglowek(); //Metoda odczytujace dane z naglowka
 
                    
                    L_nazwa.Text = "Adres:"+okienko.FileName;
                    L_typ.Text = "Typ:"+objekt.Plik.typBMP;
                    L_rozmiar.Text = "Rozmiar:"+objekt.Plik.RozmiarPliku.ToString()+" KB";
                    L_szerokosc.Text = "Szerokosc:" + objekt.Plik.szerokosc.ToString();
                    L_wysokosc.Text = "Wysokosc:" + objekt.Plik.dlugosc.ToString();
                    L_piksel.Text = "Piksel Startowy:" + objekt.Plik.PikselStartowy.ToString();

                    Obraz.Image = Image.FromFile(okienko.FileName); //Zaladowanie bitmapy do PicturBox

                    
                   
                }

                catch (Exception exc)
                {
                    MessageBox.Show(exc.ToString());
                }
                
            }
        }


        private void Emedia_Load(object sender, EventArgs e)
        {

        }

        private void L_typ_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
