﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Emedia
{
    class BMP
    {

   

        public byte[] Buffor;
        byte[] naglowek;
        public DaneBMP Plik;

        

        public BMP()
        {
            Plik = new DaneBMP();
        }

        public byte[] CzytajPlik(string nazwa)   
        {
            FileStream plik = new FileStream(nazwa, FileMode.Open);
            Buffor = new byte[plik.Length];
            plik.Read(Buffor, 0, Buffor.Length);
            plik.Close();

            return Buffor;
        }


        string TypBMP(ref byte[] tablica)
        {
            byte[] typ = new byte[2];

            for (int i = 0; i < typ.Length; i++)
                typ[i] = tablica[i];

            return Encoding.ASCII.GetString(typ);
        }

        int PobierzRozmiar()
        {
            int fileSize = (int)Buffor[5] << 24
                            | (int)Buffor[4] << 16
                            | (int)Buffor[3] << 8
                            | (int)Buffor[2];

            return fileSize;
        }

       

        int Szerokosc()
        {
            int dl = (int)Buffor[21] << 24
                            | (int)Buffor[20] << 16
                            | (int)Buffor[19] << 8
                            | (int)Buffor[18];

            return dl;


        }

        private int Dlugosc()
        {
            int dl = (int)Buffor[25] << 24
                            | (int)Buffor[24] << 16
                            | (int)Buffor[23] << 8
                            | (int)Buffor[22];

            return dl;


        }


        private int Poczatek()
        {
            int dl = (int)Buffor[13] << 24
                            | (int)Buffor[12] << 16
                            | (int)Buffor[11] << 8
                            | (int)Buffor[10];

            return dl;


        }

        
        public void Naglowek()
        {
            
            naglowek = new byte[14];

            for (int i = 0; i < naglowek.Length; i++) //
            {
                naglowek[i] = Buffor[i];
            }


            Plik.typBMP = TypBMP(ref naglowek);

            byte[] Tab_Rozmiar = new byte[4];

            for (int i = 0; i < Tab_Rozmiar.Length; i++)
            {
                Tab_Rozmiar[i] = Buffor[i+2];
            }

            Plik.RozmiarPliku = PobierzRozmiar()/1024; //Podane w KB
            
            

            // Pobieranie adresu poczatkowego

            byte[] Piksel = new byte[4];

            for (int i = 10, j = 0; j < Piksel.Length; i++, j++)
            {
               Piksel[j] = Buffor[i];
            }

           


            Plik.PikselStartowy = Poczatek();

            Plik.szerokosc = Szerokosc();

            Plik.dlugosc = Dlugosc();

            
            
        }


         
    }
}










