﻿namespace Emedia
{
    partial class Emedia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.otworz = new System.Windows.Forms.Button();
            this.L_typ = new System.Windows.Forms.Label();
            this.L_rozmiar = new System.Windows.Forms.Label();
            this.L_nazwa = new System.Windows.Forms.Label();
            this.L_szerokosc = new System.Windows.Forms.Label();
            this.L_wysokosc = new System.Windows.Forms.Label();
            this.Obraz = new System.Windows.Forms.PictureBox();
            this.L_piksel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Obraz)).BeginInit();
            this.SuspendLayout();
            // 
            // otworz
            // 
            this.otworz.Location = new System.Drawing.Point(12, 232);
            this.otworz.Name = "otworz";
            this.otworz.Size = new System.Drawing.Size(116, 38);
            this.otworz.TabIndex = 0;
            this.otworz.Text = "Otwórz";
            this.otworz.UseVisualStyleBackColor = true;
            this.otworz.Click += new System.EventHandler(this.button1_Click);
            // 
            // L_typ
            // 
            this.L_typ.AutoSize = true;
            this.L_typ.Location = new System.Drawing.Point(9, 80);
            this.L_typ.Name = "L_typ";
            this.L_typ.Size = new System.Drawing.Size(28, 13);
            this.L_typ.TabIndex = 1;
            this.L_typ.Text = "Typ:";
            this.L_typ.Click += new System.EventHandler(this.L_typ_Click);
            // 
            // L_rozmiar
            // 
            this.L_rozmiar.AutoSize = true;
            this.L_rozmiar.Location = new System.Drawing.Point(9, 116);
            this.L_rozmiar.Name = "L_rozmiar";
            this.L_rozmiar.Size = new System.Drawing.Size(48, 13);
            this.L_rozmiar.TabIndex = 2;
            this.L_rozmiar.Text = "Rozmiar:";
            // 
            // L_nazwa
            // 
            this.L_nazwa.Location = new System.Drawing.Point(12, 8);
            this.L_nazwa.Name = "L_nazwa";
            this.L_nazwa.Size = new System.Drawing.Size(116, 72);
            this.L_nazwa.TabIndex = 3;
            this.L_nazwa.Text = "Nazwa:";
            // 
            // L_szerokosc
            // 
            this.L_szerokosc.AutoSize = true;
            this.L_szerokosc.Location = new System.Drawing.Point(9, 143);
            this.L_szerokosc.Name = "L_szerokosc";
            this.L_szerokosc.Size = new System.Drawing.Size(60, 13);
            this.L_szerokosc.TabIndex = 4;
            this.L_szerokosc.Text = "Szerokość:";
            // 
            // L_wysokosc
            // 
            this.L_wysokosc.AutoSize = true;
            this.L_wysokosc.Location = new System.Drawing.Point(9, 173);
            this.L_wysokosc.Name = "L_wysokosc";
            this.L_wysokosc.Size = new System.Drawing.Size(60, 13);
            this.L_wysokosc.TabIndex = 5;
            this.L_wysokosc.Text = "Wysokość:";
            // 
            // Obraz
            // 
            this.Obraz.Location = new System.Drawing.Point(154, 8);
            this.Obraz.Name = "Obraz";
            this.Obraz.Size = new System.Drawing.Size(207, 262);
            this.Obraz.TabIndex = 6;
            this.Obraz.TabStop = false;
            // 
            // L_piksel
            // 
            this.L_piksel.AutoSize = true;
            this.L_piksel.Location = new System.Drawing.Point(12, 202);
            this.L_piksel.Name = "L_piksel";
            this.L_piksel.Size = new System.Drawing.Size(76, 13);
            this.L_piksel.TabIndex = 7;
            this.L_piksel.Text = "PikselStartowy";
            this.L_piksel.Click += new System.EventHandler(this.label1_Click);
            // 
            // Emedia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(376, 285);
            this.Controls.Add(this.L_piksel);
            this.Controls.Add(this.Obraz);
            this.Controls.Add(this.L_wysokosc);
            this.Controls.Add(this.L_szerokosc);
            this.Controls.Add(this.L_nazwa);
            this.Controls.Add(this.L_rozmiar);
            this.Controls.Add(this.L_typ);
            this.Controls.Add(this.otworz);
            this.Name = "Emedia";
            this.Text = "Otwieranie BMP";
            this.Load += new System.EventHandler(this.Emedia_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Obraz)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button otworz;
        private System.Windows.Forms.Label L_typ;
        private System.Windows.Forms.Label L_rozmiar;
        private System.Windows.Forms.Label L_nazwa;
        private System.Windows.Forms.Label L_szerokosc;
        private System.Windows.Forms.Label L_wysokosc;
        private System.Windows.Forms.PictureBox Obraz;
        private System.Windows.Forms.Label L_piksel;
    }
}

